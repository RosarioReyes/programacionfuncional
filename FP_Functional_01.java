import java.util.List;

 /*Structured Approach */

 public class FP_Functional_01{

 		public static void main(String [] args){
 			List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);

 			System.out.println("Using to --> [System.out :: print] by default") ;
 			printAllNumbersInListFunctional_Two(numbers);
 			System.out.println("\nprintAllNumbersInListFuntional:");
 			printAllNumbersInListFunctional(numbers);
 			System.out.println("\nprintEvenNumbersInListFuntional:");
 			printEvenNumbersInListFunctional(numbers);
 			System.out.println("\nprintSquaresOfNumbersInListFuntional:");
 			printSquaresOfEvenNumbersInListFunctional(numbers);
 			System.out.println("");
 		}

 		private static void print(int number){
 			System.out.print(number + ",");
 		}

 		private static boolean isEven(int number){
 			return (number % 2 == 0);
 		}

 		private static void printAllNumbersInListFunctional_Two(List<Integer>numbers){
 			    //what to do?
 			numbers.stream()  // --->convert to stream
 			.forEach(System.out::print);   //--->Method reference
 			System.out.println("");
 		}
 		private static void printAllNumbersInListFunctional(List<Integer> numbers){
 			//what to do?}
 			numbers.stream()
 			.forEach(FP_Functional_01::print);   // --->Method reference
 			System.out.println("");
 		}

 		//number -> number % 2 == 0
 		private static void printEvenNumbersInListFunctional(List<Integer>numbers){
 			//what to do?
 			numbers.stream()
 				   .filter(FP_Functional_01::isEven)
 				   .forEach(FP_Functional_01::print);
 			System.out.println("");
 		}

 		private static void printSquaresOfEvenNumbersInListFunctional(List<Integer> numbers){
 			numbers.stream()
 				   .filter(number ->  number % 2 == 0)
 				   .map(number -> number + number)
 				   .forEach(FP_Functional_01::print);
 			System.out.println("");
 		}

 	}
